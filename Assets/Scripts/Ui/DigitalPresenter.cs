using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DigitalPresenter : MonoBehaviour
{
    [SerializeField] 
    private Clock _clock;

    [SerializeField] 
    private TMP_Text _text;

    private void OnEnable()
    {
        _clock.Hour.Changed += Present;
        _clock.Minute.Changed += Present;
        _clock.Second.Changed += Present;
    }

    private void OnDisable()
    {
        _clock.Hour.Changed -= Present;
        _clock.Minute.Changed -= Present;
        _clock.Second.Changed -= Present;
    }

    private void Present(int value)
    {
        _text.text = string.Format($"{_clock.Hour.Value:D2}:{_clock.Minute.Value:D2}:{_clock.Second.Value:D2}");
    }
}
