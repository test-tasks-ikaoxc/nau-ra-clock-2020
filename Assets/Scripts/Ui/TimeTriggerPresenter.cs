using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeTriggerPresenter : MonoBehaviour
{
    [SerializeField] 
    private TimeTrigger _timeTrigger;

    [SerializeField] 
    private TMP_Text _text;
    
    private void OnEnable()
    {
        _timeTrigger.Setted += Present;
    }

    private void OnDisable()
    {
        _timeTrigger.Setted -= Present;
    }

    private void Present(TimeSpan time)
    {
        _text.text = string.Format($"{time.Hours}:{time.Minutes}:{time.Seconds}");
    }
}
