using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelPresenter : MonoBehaviour
{
    [SerializeField] 
    private GameObject[] _panels;
    private GameObject _current;

    private void Awake()
    {
        Show(0);
    }

    public void Show(int panel)
    {
        if (_current != null) 
            _current.SetActive(false);
        
        _current = _panels[panel];
        
        _panels[panel].SetActive(true);
    }
}
