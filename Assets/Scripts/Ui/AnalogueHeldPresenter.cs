using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalogueHeldPresenter : MonoBehaviour
{
    [SerializeField] 
    private DigitSettings _digitSettings;
    
    [SerializeField] 
    private Digit _digit;

    private void OnEnable()
    {
        _digit.Changed += Present;
    }

    private void OnDisable()
    {
        _digit.Changed -= Present;
    }

    private void Present(int value)
    {
        transform.localEulerAngles = new Vector3(0.0f, 0.0f, _digitSettings.ConvertToDegree(value));
    }
}
