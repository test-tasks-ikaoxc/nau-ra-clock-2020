using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmClock : MonoBehaviour
{
    [SerializeField] 
    private TimeTrigger _timeTrigger;
    
    [SerializeField]
    private AlarmHeld _hours;
    
    [SerializeField]
    private AlarmHeld _minutes;
    
    [SerializeField]
    private AlarmHeld _seconds;
    
    public void SetAlert()
    {
        _timeTrigger.SetAlert(new TimeSpan(_hours.GetTime(), _minutes.GetTime(), _seconds.GetTime()));
    }
}
