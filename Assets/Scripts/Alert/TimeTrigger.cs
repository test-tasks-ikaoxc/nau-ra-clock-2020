using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTrigger : MonoBehaviour
{
    [SerializeField]
    private Clock _clock;
    private TimeSpan _alertTime;

    public Action<TimeSpan> Setted;
    public Action Elapsed;

    private void OnEnable()
    {
        _clock.Hour.Changed += OnClockChanged;
        _clock.Minute.Changed += OnClockChanged;
        _clock.Second.Changed += OnClockChanged;
    }

    private void OnDisable()
    {
        _clock.Hour.Changed -= OnClockChanged;
        _clock.Minute.Changed -= OnClockChanged;
        _clock.Second.Changed -= OnClockChanged;
    }

    public void SetAlert(TimeSpan alertTime)
    {
        _alertTime = alertTime;
        Setted?.Invoke(alertTime);
    }
    
    private void OnClockChanged(int value)
    {
        if (TimeSpan.Compare(_clock.GetTime(), _alertTime) == 0)
        {
            Elapsed?.Invoke();
        }
    }
}
