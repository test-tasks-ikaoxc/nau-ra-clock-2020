using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DigitalInput : MonoBehaviour
{
    [SerializeField] 
    private TimeTrigger _timeTrigger;
    
    [SerializeField] 
    private TMP_InputField _hour;
    [SerializeField] 
    private TMP_InputField _minute;
    [SerializeField] 
    private TMP_InputField _second;

    public void SetAlert()
    {
        // TODO: VALIDATION, INPUT FIELD
        
        _timeTrigger.SetAlert(new TimeSpan(int.Parse(_hour.text), int.Parse(_minute.text), int.Parse(_second.text)));
    }
}
