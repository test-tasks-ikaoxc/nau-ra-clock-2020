using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeldToCursorRotate : MonoBehaviour, IDragHandler
{ 
    private Vector2 _endPoint;

    private void Update()
    {
        var look = _endPoint - (Vector2) transform.position;
        var rotz = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, (rotz - 90));
    }

    public void OnDrag(PointerEventData eventData)
    {
        _endPoint = eventData.position;
    }
}
