using System;
using UnityEngine;

public class AlarmHeld : MonoBehaviour
{
    [SerializeField] 
    private DigitSettings _digitSettings;
    
    public int GetTime()
    {
        return _digitSettings.ConvertFromDegree(transform.localEulerAngles.z);
    }
}