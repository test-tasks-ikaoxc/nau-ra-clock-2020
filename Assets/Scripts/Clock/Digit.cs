using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Digit : MonoBehaviour
{
    [SerializeField]
    private DigitSettings _digitSettings;
    
    private int _value;
    public int Value => _value;

    public Action<int> Changed;
    public Action Overlapped;

    public void Next() => Set(_value + 1);
    public void Set(int value)
    {
        if (value >= _digitSettings.Max)
            Overlapped?.Invoke();
        
        _value = (int) Mathf.Repeat(value, _digitSettings.Max);
        
        Changed?.Invoke(_value);
    }
}
