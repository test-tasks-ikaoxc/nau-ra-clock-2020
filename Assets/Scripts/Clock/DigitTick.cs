using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigitTick : MonoBehaviour
{
    [SerializeField] 
    private Digit _digit;
    private Coroutine _coroutine;
    
    private void OnEnable()
    {
        _digit.Changed += OnDigitChanged;
    }

    private void OnDisable()
    {
        _digit.Changed -= OnDigitChanged;
    }

    private IEnumerator Tick()
    {
        yield return new WaitForSecondsRealtime(1f);
        _digit.Next();
    }

    private void OnDigitChanged(int value)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = StartCoroutine(Tick());
    }
}
