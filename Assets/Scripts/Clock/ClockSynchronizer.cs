using System;
using System.Collections;
using UnityEngine;

public class ClockSynchronizer : MonoBehaviour
{
    [SerializeField] 
    private Clock _clock;
    private Coroutine _coroutine;

    private void Start()
    {
        TrySync();
    }

    private void OnEnable()
    {
        _clock.Hour.Changed += OnHourChanged;
    }

    private void OnDisable()
    {
        _clock.Hour.Changed -= OnHourChanged;
    }

    private IEnumerator Sync()
    {
        yield return new WaitForSecondsRealtime(3600);
        TrySync();
    }

    private void TrySync()
    {
        try
        {
            _clock.SetTime(NtpClient.GetNetworkTime());
        }
        catch
        {
            _clock.SetTime(DateTime.Now);
        }    
    }
    
    private void OnHourChanged(int value)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = StartCoroutine(Sync());
    }
}
