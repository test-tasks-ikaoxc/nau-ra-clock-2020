using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    [SerializeField] 
    private Digit _hour;
    public Digit Hour => _hour;
    
    [SerializeField] 
    private Digit _minute;
    public Digit Minute => _minute;
    
    [SerializeField] 
    private Digit _second;
    public Digit Second => _second;

    private void OnEnable()
    {
        _minute.Overlapped += OnMinuteOverlapped;
        _second.Overlapped += OnSecondOverlapped;
    }

    private void OnDisable()
    {
        _minute.Overlapped -= OnMinuteOverlapped;
        _second.Overlapped -= OnSecondOverlapped;
    }

    public TimeSpan GetTime() => new TimeSpan(_hour.Value, _minute.Value, _second.Value);

    public void SetTime(DateTime dateTime)
    {
        Hour.Set(dateTime.Hour);
        Minute.Set(dateTime.Minute);
        Second.Set(dateTime.Second);
    }

    private void OnMinuteOverlapped() => _hour.Next();
    private void OnSecondOverlapped() => _minute.Next();


}
