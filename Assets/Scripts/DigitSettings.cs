using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class DigitSettings : ScriptableObject
{
    public int Max;
    public int DegreesPerOne;

    public float ConvertToDegree(int value)
    {
        return (Max - value) * (360.0f / Max);
    }

    public int ConvertFromDegree(float degree)
    {
        var degreesPerHalf = Max / 2;
        
        var value = degree switch
        {
            > 0 => degreesPerHalf + (degreesPerHalf - Math.Abs(degree) / DegreesPerOne),
            < 0 => degreesPerHalf - Math.Abs(degree) / DegreesPerOne,
            _ => 0
        };

        return (int) value;
    }
}
